// Function
/*
	
	Syntax:
		function functioName(){
			code block (statement)
		}
*/
function printName(){
	console.log("My name is Juls");
};

printName();

// Function declaration vs expressions

// Function Declaration
function declaredFunction(){
	console.log("Hello, World from declaredFunction()");
};

declaredFunction();

// Function Expression
let variableFunction = function(){
	console.log("Hello again!");
};

variableFunction();

declaredFunction = function(){
	console.log("Updated declaredFunction");
};

declaredFunction();

const constantFunc = function(){
	console.log("Initialized with const!");
};

constantFunc();

/*constantFunc = function{
	console.log("Cannot be re-assigned");
};*/

// Function scoping

/*
	Scope is the accessibility (visibility) of variables

	JS Variables has 3 types of scope:
		1. local/block scope
		2. global scope
		3. function scope
*/
// Local or Block scope example
{
	let localVar = "Alonzo Mattheo";
	console.log(localVar);
}

// global scope example
let globalVar = "Aizaac Ellis";

console.log(globalVar);

// function scope example
function showNames() {
	// function scoped variables
	var functionVar = "Joe";
	const functionConst = "John";
	let functionLet = "Jane";

	console.log(functionVar);
	console.log(functionConst);
	console.log(functionLet);
}

showNames();

// Nested Functions

function myNewFunction() {
	let name = "Jane";

	function nestedFunction() {
		let nestedName = "John";
		console.log(name);
	}
	nestedFunction();
}
myNewFunction();

// Function and Global scoped variables

// Global scope variable

let globalName = "Joy";

function myNewFunction2() {
	let nameInside = "Kenzo";

	console.log(globalName);
}
myNewFunction2();

// Using alert()

alert("Hello, World!"); // This will run immediately when the page loads

function showSampleAlert () {
	alert("Hello, User!");
}

showSampleAlert();

// Using prompt()

let samplePrompt = prompt("Enter your name: ");

console.log("Hello, " + samplePrompt);

function printWelcomeMessage() {
	let firstName = prompt("Enter your first name: ");
	let lastName = prompt("Enter your last name: ");

	console.log("Hello " + firstName + lastName);
	console.log("Welcome to my Page!");
}
printWelcomeMessage();